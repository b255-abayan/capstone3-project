import Container from "react-bootstrap/Container";
import { useContext, useState, useEffect } from "react";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";
import { Link, NavLink } from "react-router-dom";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import React from "react";
import UserContext from "../userContext";

export default function AppNavbar() {
  // State Hook for the user state thats deficed here for a global scope

  const { user, setUser } = useContext(UserContext);
  // console.log(user);
  // const [user, setUser] = useState({
  //   // email: localStorage.getItem("email"),
  //   id: null,
  //   isAdmin: null,
  // });

  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    // console.log(user);
    // console.log(localStorage);
  }, [user]);

  return (
    <Navbar
      collapseOnSelect
      expand="lg"
      bg="trasparent"
      variant="dark"
      style={{ backgroundColor: "#3653A3" }}
    >
      <Container fluid>
        <Navbar.Brand as={NavLink} to="/">
          <img
            src="/web-logo.png"
            width="60"
            height="60"
            className="d-inline-block align-top"
            alt="React Bootstrap logo"
          />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="me-auto my-2 my-lg-0"
            style={{ maxHeight: "100px" }}
            navbarScroll
          >
            {/* <Nav.Link as={NavLink} to="/">
              Home
            </Nav.Link>
            <Nav.Link as={NavLink} to="/products" exact="true">
              Products
            </Nav.Link> */}
          </Nav>
          <Nav>
            {user.id !== null ? (
              <NavDropdown
                title={`${user.firstName} ${user.lastName}`}
                id="collasible-nav-dropdown"
              >
                <NavDropdown.Item as={NavLink} to="/profile" exact="true">
                  Profile
                </NavDropdown.Item>

                {localStorage.isAdmin === "true" ? (
                  <NavDropdown.Item
                    as={NavLink}
                    to="/admin/products"
                    exact="true"
                  >
                    Manage Application
                  </NavDropdown.Item>
                ) : (
                  // <Link to="/admin/products" className="">
                  //   {" Create Account"}
                  // </Link>
                  false
                )}
                <NavDropdown.Item as={NavLink} to="/logout" exact="true">
                  Logout
                </NavDropdown.Item>
              </NavDropdown>
            ) : (
              <React.Fragment>
                <Nav.Link as={NavLink} to="/login" exact="true">
                  Login
                </Nav.Link>
                <Nav.Link as={NavLink} to="/register" exact="true">
                  Register
                </Nav.Link>
              </React.Fragment>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
