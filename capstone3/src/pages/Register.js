import { Form, Button } from "react-bootstrap";
import { useEffect, useState, useContext } from "react";
import UserContext from "../userContext";
import { Navigate, useNavigate } from "react-router-dom";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";

export default function Register() {
  const navigate = useNavigate();
  // const { user, setUser } = useContext(UserContext);
  const { user } = useContext(UserContext);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [isActive, setisActive] = useState(false);

  // console.log(firstName);
  // console.log(lastName);
  // console.log(email);
  // console.log(mobileNumber.length);
  // console.log(password1);
  // console.log(password2);

  // function to simulate user registration
  const registerUser = async (e) => {
    e.preventDefault();
    //check if the email is already exist
    const response = await fetch(
      `${process.env.REACT_APP_API_URL}/users/checkEmail`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: email,
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          Swal.fire({
            title: "Duplicate email found",
            icon: "error",
            text: "Please provide a different email",
          });
        } else {
          // If no email duplicate proceed with the registration
          register();
        }
      });

    setFirstName("");
    setLastName("");
    setEmail("");
    setPassword1("");
    setPassword2("");
  };

  const register = async () => {
    const response = await fetch(
      `${process.env.REACT_APP_API_URL}/users/register`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          firstName: firstName,
          lastName: lastName,
          email: email,
          password: password2,
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        // If post request is successfully executed with true response, fire the success alert and redirect the page to "login"
        if (data.message === "User created successfully") {
          Swal.fire({
            title: "Registration Successful",
            icon: "success",
            text: "Welcome to Ecommerce!",
          });
          // Redirect to login page upon successful registration
          navigate("/login");
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again",
          });
        }
      });
  };
  useEffect(() => {
    if (
      firstName !== "" &&
      lastName !== "" &&
      email !== "" &&
      password1 !== "" &&
      password2 !== "" &&
      password1 === password2
    ) {
      setisActive(true);
    } else {
      setisActive(false);
    }
  }, [firstName, lastName, email, password1, password2]);

  return user.id !== null ? (
    <Navigate to="/" />
  ) : (
    <Container>
      <Row>
        <Col></Col>
        <Col sm>
          <Card className="mb-2 bg-dark text-light mt-5">
            <Card.Header>
              <h1>Registration</h1>
            </Card.Header>
            <Card.Body>
              <Form onSubmit={(e) => registerUser(e)}>
                <Row>
                  <Col>
                    <Form.Group controlId="firstName" className="mt-3">
                      <Form.Label>First Name</Form.Label>
                      <Form.Control
                        type="text"
                        placeholder="First Name"
                        value={firstName}
                        onChange={(e) => setFirstName(e.target.value)}
                        required
                      />
                    </Form.Group>
                  </Col>
                  <Col>
                    <Form.Group controlId="lastName" className="mt-3">
                      <Form.Label>Last Name</Form.Label>
                      <Form.Control
                        type="text"
                        placeholder="Last Name"
                        value={lastName}
                        onChange={(e) => setLastName(e.target.value)}
                        required
                      />
                    </Form.Group>
                  </Col>
                </Row>
                <Form.Group controlId="userEmail" className="mt-3">
                  <Form.Label>Email address</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                  />
                </Form.Group>
                <Form.Group controlId="password1" className="mt-3">
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    type="password"
                    placeholder="Password"
                    value={password1}
                    onChange={(e) => setPassword1(e.target.value)}
                    required
                  />
                </Form.Group>
                <Form.Group controlId="password2" className="mt-3">
                  <Form.Label>Verify Password</Form.Label>
                  <Form.Control
                    type="password"
                    placeholder="Verify Password"
                    value={password2}
                    onChange={(e) => setPassword2(e.target.value)}
                    required
                  />
                </Form.Group>
                <br />
                {isActive ? (
                  <Button variant="warning" type="submit" id="submitBtn">
                    Sign Up
                  </Button>
                ) : (
                  <Button
                    variant="warning"
                    type="submit"
                    id="submitBtn"
                    disabled
                  >
                    Sign Up
                  </Button>
                )}
                <br /> <br />
                <pre>
                  Already have an account?
                  <Link to="/login" className="text-light">
                    {" Sign In"}
                  </Link>
                </pre>
              </Form>
            </Card.Body>
          </Card>
        </Col>
        <Col></Col>
      </Row>
    </Container>
  );
}
