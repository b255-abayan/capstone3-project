import { useState, useEffect, useContext } from "react";
import { Form, Button } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../userContext";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import { Link } from "react-router-dom";

export default function Login() {
  const API_URL = process.env.REACT_APP_API_URL;
  //Allows us to consume the User context object and it's properties to use for user validation
  const { user, setUser } = useContext(UserContext);

  // State hooks to store the values of the input fields
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  // State to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(true);

  const authenticate = async (e) => {
    // Prevents page redirection via form submission
    e.preventDefault();

    // Process a fetch request to the corresponding backend API
    // The header information "Content-Type" is used to specify that the information being sent to the backend will be sent in the form of JSON
    // The fetch request will communicate with our backend application providing it with a stringified JSON
    try {
      const response = await fetch(`${API_URL}/users/login`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: email,
          password: password,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          // console.log(data);
          if (data.auth !== "Authentication Failed") {
            localStorage.setItem("token", data.access);
            retrieveUserDetails(data.access);
            Swal.fire({
              title: "Login Successful",
              icon: "success",
              text: "Welcome to Ecommerce!",
            });
          } else {
            Swal.fire({
              title: "Authentication Failed",
              icon: "error",
              text: "Check your login details and try again.",
            });
          }
        });
      // Clear input fields after submission
      setEmail("");
      setPassword("");
    } catch (error) {
      console.log(error);
    }
  };

  const retrieveUserDetails = async (token) => {
    // The token will be sent as part of the requests header information
    try {
      const response = await fetch(`${API_URL}/users/loggedin`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          localStorage.setItem("id", data[0]._id);
          localStorage.setItem("isAdmin", data[0].isAdmin);
          localStorage.setItem("firstName", data[0].firstName);
          localStorage.setItem("lastName", data[0].lastName);
          setUser({
            id: data[0]._id,
            isAdmin: data[0].isAdmin,
            firstName: data[0].firstName,
            lastName: data[0].lastName,
          });
        });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    // Validation to enable submit button when all fields are populated and both passwords match
    // console.log(localStorage);
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return user.id !== null ? (
    <Navigate to="/" />
  ) : (
    <Container>
      <Row>
        <Col></Col>
        <Col sm>
          <Card className="mb-2 bg-dark text-light mt-5">
            <Card.Header>
              <h1>Authenticate</h1>
            </Card.Header>
            <Card.Body>
              <Form onSubmit={(e) => authenticate(e)}>
                <Form.Group controlId="userEmail" className="mt-3">
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                  />
                </Form.Group>
                <Form.Group controlId="password" className="mt-3">
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    type="password"
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                  />
                </Form.Group>
                {isActive ? (
                  <Button
                    variant="warning"
                    type="submit"
                    id="submitBtn"
                    className="mt-3"
                  >
                    Sign In
                  </Button>
                ) : (
                  <Button
                    variant="warning"
                    type="submit"
                    id="submitBtn"
                    disabled
                    className="mt-3"
                  >
                    Sign In
                  </Button>
                )}
                <br /> <br />
                <pre>
                  Don't have an account?
                  <Link to="/register" className="text-light">
                    {" Create Account"}
                  </Link>
                </pre>
              </Form>
            </Card.Body>
          </Card>
        </Col>
        <Col></Col>
      </Row>
    </Container>
  );
}
