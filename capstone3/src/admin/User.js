import SideNav from "./components/SideNav";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import UserContext from "../userContext";
import { useContext } from "react";
import { Navigate } from "react-router-dom";
export default function Product() {
  const { user, setUser } = useContext(UserContext);

  const fetchUsers = async () => {
    try {
      if (localStorage.id !== null && localStorage.isAdmin === "true") {
        const response = await fetch(`${process.env.REACT_APP_API_URL}/users`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.token}`,
          },
        })
          .then((res) => {
            return res.json();
          })
          .then((data) => {
            data.forEach((element) => {
              const markup = `
              <tr>
                <td>${element.firstName}</td>
                <td>${element.lastName}</td>
                <td>${element.email}</td>
                <td>${
                  element.isAdmin
                    ? "<span class='text-success'>Admin</span>"
                    : "<span class='text-danger'>User</span>"
                }</td>
                <td>
                  <a href="/admin/products/update/${
                    element._id
                  }" class="">Update</a>
                  <a href="/admin/products/delete/${
                    element._id
                  }" class="">Delete</a>
                </td>
              </tr>
              `;
              document
                .querySelector("#users")
                .insertAdjacentHTML("beforeend", markup);
            });
          });
      }
    } catch (error) {
      console.log(error);
    }
  };

  fetchUsers();

  return (
    <div className="row p-3">
      <div className="col-sm-2 ">
        <SideNav />
      </div>
      <div className="col-sm-10  p-3">
        <Link style={{ textDecoration: "none" }} to="/admin/products/create">
          {/* <Button className="btn btn-sm bg-success">Add Product +</Button> */}
        </Link>
        <h1>User List</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th scope="col">First Name</th>
              <th scope="col">Last Name</th>
              <th scope="col">Email</th>
              <th scope="col">Access Privilege</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody id="users"></tbody>
        </table>
      </div>
    </div>
  );
}
