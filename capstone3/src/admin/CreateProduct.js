import SideNav from "./components/SideNav";
import { Link } from "react-router-dom";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import { Form, Button } from "react-bootstrap";
import FloatingLabel from "react-bootstrap/FloatingLabel";
import { useEffect, useState } from "react";
import Swal from "sweetalert2";
import UserContext from "../userContext";
import { useContext } from "react";
import { Navigate } from "react-router-dom";

export default function User() {
  const { user, setUser } = useContext(UserContext);
  const [name, setName] = useState("");
  let [price, setPrice] = useState("");
  const [description, setDescription] = useState("");
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    if (name !== "" && price !== "" && description !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  });

  function clearFields() {
    setName("");
    setPrice("");
    setDescription("");
  }

  const create = async (e) => {
    try {
      e.preventDefault();
      if (localStorage.id !== null && localStorage.isAdmin === "true") {
        const response = await fetch(
          `${process.env.REACT_APP_API_URL}/products`,
          {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${localStorage.token}`,
            },
            body: JSON.stringify({
              name: name,
              price: parseInt(price),
              description: description,
            }),
          }
        )
          .then((res) => res.json())
          .then((data) => {
            if (data.message == "Product created successfully") {
              clearFields();
              Swal.fire({
                title: "Successful",
                icon: "success",
                text: "Product create successfully",
              });
            } else {
              clearFields();
              Swal.fire({
                title: "Failed",
                icon: "error",
                text: "Check the details and try again",
              });
            }
          });
      } else {
        clearFields();
        Swal.fire({
          title: "Failed",
          icon: "error",
          text: "Something wen't wrong",
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="row p-3">
      <div className="col-sm-2 ">
        <SideNav />
      </div>
      <div className="col-sm-10 p-3">
        <Container>
          <Row>
            <Col sm>
              <Card
                className="mb-2 text-light"
                style={{ backgroundColor: "#3653A3" }}
              >
                <Card.Header>
                  <h1>Create Product</h1>
                </Card.Header>
                <Card.Body>
                  <Form onSubmit={(e) => create(e)}>
                    <Form.Group controlId="productName" className="mt-3">
                      <Form.Label>Name</Form.Label>
                      <Form.Control
                        type="text"
                        placeholder="Product Name"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                      />
                    </Form.Group>
                    <Form.Group controlId="productPrice" className="mt-3">
                      <Form.Label>Price</Form.Label>
                      <Form.Control
                        type="number"
                        placeholder="Price"
                        value={price}
                        onChange={(e) => setPrice(e.target.value)}
                      />
                    </Form.Group>
                    <Form.Label className="mt-3">Description</Form.Label>
                    <FloatingLabel
                      controlId="floatingTextarea2"
                      label="Comments"
                    >
                      <Form.Control
                        as="textarea"
                        placeholder="Leave a comment here"
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                        style={{ height: "100px" }}
                      />
                    </FloatingLabel>
                    {isActive ? (
                      <Button
                        variant="warning"
                        type="submit"
                        id="submitBtn"
                        className="mt-3"
                      >
                        Create
                      </Button>
                    ) : (
                      <Button
                        variant="warning"
                        type="submit"
                        id="submitBtn"
                        disabled
                        className="mt-3"
                      >
                        Create
                      </Button>
                    )}
                    <br /> <br />
                  </Form>
                  <Link style={{ textDecoration: "none" }} to="/admin/products">
                    <p className="text-white p-3"> {"<< Back to Products"}</p>
                  </Link>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
}
