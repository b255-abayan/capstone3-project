import { Link } from "react-router-dom";
export default function SideNav() {
  return (
    <ul className="list-group" style={{ backgroundColor: "#3653A3" }}>
      <Link style={{ textDecoration: "none" }} to="/admin/products">
        <li
          className="list-group-item"
          style={{ backgroundColor: "#3653A3", color: "white" }}
        >
          Products
        </li>
      </Link>

      <Link style={{ textDecoration: "none" }} to="/admin/users">
        <li
          className="list-group-item"
          style={{ backgroundColor: "#3653A3", color: "white" }}
        >
          Users
        </li>
      </Link>
    </ul>
  );
}
