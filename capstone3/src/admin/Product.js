import SideNav from "./components/SideNav";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import UserContext from "../userContext";
import { useContext } from "react";
import { Navigate } from "react-router-dom";

export default function Product() {
  const { user, setUser } = useContext(UserContext);

  const fetchProduct = async () => {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/products/all`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
          },
        }
      )
        .then((res) => {
          return res.json();
        })
        .then((data) => {
          data.forEach((element) => {
            const markup = `
      <tr>
        <td>${element.name}</td>
        <td>${element.price}</td>
        <td>${
          element.isActive
            ? "<span class='text-success'>Yes</span>"
            : "<span class='text-danger'>No</span>"
        }</td>
        <td>
        

        ${
          element.isActive
            ? '<a href="/admin/products/update/$" class="text-success"><i class="bi bi-eye"></i></a>'
            : '<a href="/admin/products/update/$" class="text-danger"><i class="bi bi-eye-slash"></i></a>'
        }

          <a href="#" class="text-warning"><i class="bi bi-pencil-square"></i></a>
          



          
          <a href="/admin/products/delete/${
            element._id
          }" class="text-danger"><i class="bi bi-trash"></i></a>
        </td>
      </tr>
      `;
            document
              .querySelector("#products")
              .insertAdjacentHTML("beforeend", markup);
          });
        });
    } catch (error) {
      console.log(error);
    }
  };
  fetchProduct();

  return (
    <div className="row p-3">
      <div className="col-sm-2 ">
        <SideNav />
      </div>
      <div className="col-sm-10  p-3">
        <Link style={{ textDecoration: "none" }} to="/admin/products/create">
          <Button className="btn btn-sm bg-success">Add Product +</Button>
        </Link>
        <h1>Product List</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Price</th>
              <th scope="col">Active</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody id="products"></tbody>
        </table>
      </div>
      <a href="#" className="text-success">
        <i className="bi bi-eye"></i>
      </a>
    </div>
  );
}
