import "./App.css";
import { UserProvider } from "./userContext";
import { useState, useEffect } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { Route, Routes } from "react-router-dom";
import { Container } from "react-bootstrap";
import AppNavBar from "./components/AppNavBar";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";
import Logout from "./pages/Logout";
import Product from "./admin/Product";
import CreateProduct from "./admin/CreateProduct";
import User from "./admin/User";
import { Navigate } from "react-router-dom";
function App() {
  // State Hook for the user state thats deficed here for a global scope
  const [user, setUser] = useState({
    id: localStorage.getItem("id"),
    isAdmin: localStorage.getItem("isAdmin"),
    firstName: localStorage.getItem("firstName"),
    lastName: localStorage.getItem("lastName"),
  });

  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  };

  // if (user.id !== null && user.isAdmin === true) {
  //   console.log("LoggedIn");
  // } else {
  //   console.log("LoggedOut");
  // }

  useEffect(() => {}, [user]);
  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <Container>
          {localStorage.id !== "null" ? <AppNavBar /> : false}

          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/logout" element={<Logout />} />
            {/* Admin Routes */}

            <Route path="/admin/products" element={<Product />} />
            <Route path="/admin/products/create" element={<CreateProduct />} />
            <Route path="/admin/users" element={<User />} />
            <Route path="*" element={<h1>No routes found</h1>} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
